# GigaBinCompressor2020
##### V&V Project
##### Authors : Victor HARABARI - Nathanaël TOUCHARD
##### Group : M2 - ILA

## Environnement

-   C++17

## Tools
Cmake
lcov

## Introduction
The application to develop is a command line tool to compress and decompress a given binary file.

## Commands
```
/usr/bin/cmake --build /home/vharabari/Work/istic/vv/gigabincompressor2020/build --config Debug --target TestTarget -- -j 10

/usr/bin/cmake --build /home/vharabari/Work/istic/vv/gigabincompressor2020/build --config Debug --target ccov-TestTarget -- -j 10

/usr/bin/cmake --build /home/vharabari/Work/istic/vv/gigabincompressor2020/build --config Debug --target TestTarget -- -j 10

/usr/bin/ctest -j10 -C Debug -T test --output-on-failure
```

## Solutions

For unit testing, we used Cmake's Catch library. This allows us to write tests in C ++ which are easy to use and easily programmable. It makes it possible to write the scenario and to specify the expected software behaviors in an understandable logical language. It can be compared to Cucumber.

We have use ccov library for the code coverage.This provides the information necessary for code coverage and generates a display of the results in an accessible report.

## Difficulties

The main difficulty was to find and integrate unit tests and code coverage in C ++.

## Conclusion

The project is rich in understanding the design of validation tests. It was a challenge to try to set it up in C ++ rather than in Java, because in Java it is quite trivial and we have already done it.