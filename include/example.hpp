#include <stack>

class basic {
  public:
    void add(int i);

    int remove();

    int size();

  protected:
    std::stack<int> _stack;
};