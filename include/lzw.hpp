#ifndef LZW_HPP
#define LZW_HPP
#include <iostream>
#include <limits>
#include <map>
#include <vector>
namespace vv {
using namespace std::string_literals;

class lzw {
  public:
    /**
     * PSEUDOCODE :
     *  w = NIL;
        while ( read a character k )
            {
                if wk exists in the dictionary
                w = wk;
                else
                add wk to the dictionary;
                output the code for w;
                w = k;
            }
    */

    static void encode(std::istream &in, std::ostream &out) {
        std::map<std::string, u_short> s2i_table;
        u_short i = 0;
        for (i = 0; i < CHAR_LIMIT; i++)
            s2i_table[std::string(1, i)] = i;
        std::string w("");
        char k;
        while (!in.eof()) {
            in >> k;
            std::string wk = w + k;
            if (s2i_table.count(wk)) {
                w = wk;
            } else {
                s2i_table[wk] = i++; // increment i AFTER it's used
                auto result = short_to_chars(s2i_table[w]);
                out << result;
                w = k;
            }
            out.flush();
        }
    }

    /**
     * PSEUDOCODE :
     *  read a character k;
        output k;
        w = k;
        while ( read a character k )
        // k could be a character or a code.
            {
                entry = dictionary entry for k;
                output entry;
                add w + entry[0] to dictionary;
                w = entry;
            }
     */
    static void decode(std::istream &in, std::ostream &out) {
        std::map<u_short, std::string> i2s_table;
        int i = 0;
        for (i = 0; i < CHAR_LIMIT; i++)
            i2s_table[i] = std::string(1, i);
        u_char kc = 0;
        u_short k = 0;
        std::string w = "";
        in >> kc;
        out << kc;
        w = std::string(1,kc);
        while (!in.eof()) {
            in >> kc;
            k = kc;
            if (kc == ESCAPE_CHAR) {
                in >> kc;
                if (kc == ESCAPE_CHAR) {
                    k = kc;
                } else {
                    k = kc << 8;
                    in >> kc;
                    k += kc;
                }
            }
            auto entry = i2s_table[k];
            out << entry;
            auto temp = w + entry[0];
            i2s_table[i] = temp;
            i++;
            w = entry;
        }
    }

  protected:
    /**
     * convers a short to char representation
     */
    static inline std::string short_to_chars(short t) {
        if (t == ESCAPE_CHAR)
            return "\\\\"s;
        if (t < CHAR_LIMIT)
            return std::string(1, char(t));
        else
            return std::string({ESCAPE_CHAR, char(t >> 8), char(t)});
    }

    static const char ESCAPE_CHAR = '\\';
    static const int CHAR_LIMIT = std::numeric_limits<char>::max();
};

}; // namespace vv

#endif // LZW_HPP