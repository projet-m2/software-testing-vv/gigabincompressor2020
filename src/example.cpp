#include "example.hpp"

void basic::add(int i) { _stack.push(i); }

int basic::remove() {
    int val = _stack.top();
    _stack.pop();
    return val;
}

int basic::size(){
    return _stack.size();
}