#include <fstream>
#include <iostream>
#include <vector>

#include <lzw.hpp>
#include <readline/history.h>
#include <readline/readline.h>

int main(int argc, char **argv) {
    // std::ifstream ifs("input.txt", std::ios::binary);
    // std::ofstream ofs("enc.txt", std::ios::binary);
    
    std::ifstream ifs("enc.txt", std::ios::in | std::ios::binary);
    std::ofstream ofs("dec.txt", std::ios::out | std::ios::binary);
    
    ifs.unsetf(std::ios::skipws);
    ofs.unsetf(std::ios::skipws);

    // vv::lzw::encode(ifs, ofs);

    vv::lzw::decode(ifs, ofs);

    return 0;
}
