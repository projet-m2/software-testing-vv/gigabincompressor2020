#define CATCH_CONFIG_MAIN
#include "../lib/Catch2/single_include/catch2/catch.hpp"
#include "example.hpp"

SCENARIO("elements can be added to the end of a basic", "[basic]") {
    GIVEN("an empty LinkedList") {
        basic list;

        WHEN("an element is added") {
            list.add(4);

            THEN("the length increases by 1") {
                REQUIRE(list.size() == 1);
            }

            THEN("the element is added at index 0") {
                REQUIRE(list.remove() == 4);
            }
        }
    }
}